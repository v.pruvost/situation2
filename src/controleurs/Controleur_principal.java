package controleurs;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import classes.*;
import dao.*;
import vues.*;

//  écoute évènement qui peuvent se produire sur la vue
public class Controleur_principal implements ActionListener {


	// définition de l’objet instance de MaVue (vue principale)
	private Vue_principal vue;
	
	// déclaration des objets Modele qui permettront d’obtenir ou de transmettre les données :
	private DAO<Jeu> gestionJeu1;
	private JeuDAO gestionJeu;
	
	// déclarations des éventuelles propriétés utiles au contrôleur
	private List<Jeu> lesJeux;

	
	// Constructeur
	public Controleur_principal()
	{
	this.gestionJeu1 = new JeuDAO();
	this.lesJeux=gestionJeu1.recupAll();
	// + initialisations éventuelles autres propriétés définies dans cette classe
	}
	
	@Override
	// plusieurs élément sollicite controleur donc test origine évènement
	public void actionPerformed(ActionEvent e) // Méthode qu'il faut implémenter
	{
		// quand on clique sur Quitter
		if (e.getActionCommand().equals("quit"))
		{
			Quitter(); // déconnection bdd et arrêt de l’application
		}
		
			// insertion d’un nouveau jeu dans la bdd en récupérant les données de la vue
			
		else if (e.getActionCommand().equals("submit")) {

			gestionJeu1.create(vue.action_enregistrer());
		
		}
		
		
	}
	
	// ajoute vue-principale avec lequelle il est lié (appelé au lancement de l'application)
		public void setVue_principal(Vue_principal view) {
			this.vue = view;
			
			
		}

	
	
	public void Quitter(){
		ConnexionPostgreSQL.deconnecter();
		System.exit(0);
		
	}
}

