package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;


public class ConnexionPostgreSQL {

	static String url;
	static Connection connect;
	
	public static Connection getInstance() {
		if(connect == null){
			try {
			// driver que l'on utilise
			Class.forName("org.postgresql.Driver");
			
			// identifiant BDD
			String url = "jdbc:postgresql://172.16.0.57:5432/ppe2";
		//	String url ="jdbc:postgresql://www.bts-malraux72.net:5432/ppe2";
			// connexion BDD qui retourne connect (lié à texte requête)
			connect = DriverManager.getConnection(url, "ppe2", "P@ssword");

			
			} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
			}
			}
			return connect;
			}


	public static void deconnecter() {
		
		try {
			connect.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	
	}
	
}
