package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import classes.*;

//définition de la classe d'accès aux données de la table jeux_video.jeu
public class JeuDAO extends DAO<Jeu> {

	@Override
	public List<Jeu> recupAll() {
		// définition de la liste qui sera retournée en fin de méthode
		List<Jeu> listeJeu = new ArrayList<Jeu>(); 
		
		// déclaration de l'objet qui servira pour la requète SQL
		try {
			Statement requete = this.connect.createStatement();

			// définition de l'objet qui récupère le résultat de l'exécution de la requète
			ResultSet curseur = requete.executeQuery("select * from \"jeux_video\".jeu");

			// tant qu'il y a une ligne "résultat" à lire
			while (curseur.next()){
				// objet pour la récup d'une ligne de la table Jeu
				Jeu unJeu = new Jeu();
				JeuDAO ca = new JeuDAO();				
				
				unJeu.setId_Jeu(curseur.getInt("id_Jeu"));
				unJeu.setDescription(curseur.getString("description"));
				unJeu.setDate_creation(curseur.getDate("date_creation").toLocalDate());
				unJeu.setImage(curseur.getString("image"));
				unJeu.setPrix(curseur.getDouble("prix"));
				unJeu.setCpu(curseur.getString("cpu"));
				unJeu.setSe(curseur.getString("se"));
				unJeu.setRam(curseur.getString("ram"));
				unJeu.setCarte_graphique(curseur.getString("carte_graphique"));
				unJeu.setHdd(curseur.getString("hdd"));
				unJeu.setNom(curseur.getString("nom"));
				listeJeu.add(unJeu);
			}
					
			curseur.close();
			requete.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 

		return listeJeu; 
	}
	
	public List<Jeu> recupAllJeu(String id_jeu) {
		// définition de la liste qui sera retournée en fin de méthode
		List<Jeu> listeJeu = new ArrayList<Jeu>(); 
		
		// déclaration de l'objet qui servira pour la requète SQL
		try {
			Statement requete = this.connect.createStatement();

			// définition de l'objet qui récupère le résultat de l'exécution de la requète
			ResultSet curseur = requete.executeQuery("SELECT * FROM \"jeux_video\".jeu WHERE id_jeu = '" + id_jeu +"'");

			// tant qu'il y a une ligne "résultat" à lire
			while (curseur.next()){
				// objet pour la récup d'une ligne de la table Club
				Jeu unJeu = new Jeu();
				JeuDAO ca = new JeuDAO();				
				
				unJeu.setId_Jeu(curseur.getInt("id_Jeu"));
				unJeu.setDescription(curseur.getString("description"));
				unJeu.setDate_creation(curseur.getDate("date_creation").toLocalDate());
				unJeu.setImage(curseur.getString("image"));
				unJeu.setPrix(curseur.getDouble("prix"));
				unJeu.setCpu(curseur.getString("cpu"));
				unJeu.setSe(curseur.getString("se"));
				unJeu.setRam(curseur.getString("ram"));
				unJeu.setCarte_graphique(curseur.getString("carte_graphique"));
				unJeu.setHdd(curseur.getString("hdd"));
				unJeu.setNom(curseur.getString("nom"));
				listeJeu.add(unJeu);
			}
					
			curseur.close();
			requete.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 

		return listeJeu; 
	}
	
	
	// Insertion d'un objet Jeu dans la table Jeu (1 ligne)
	@Override
	public void create(Jeu obj) {
		try {
			 	PreparedStatement prepare = this.connect
	                                        .prepareStatement("INSERT INTO \"jeux_video\".jeu (description, date_creation, image, prix, cpu, se, ram, carte_graphique, hdd, nom) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
	                                         );
			 //	prepare.setInt(1, obj.getId_Jeu());
				prepare.setString(1, obj.getDescription());
				prepare.setDate(2, java.sql.Date.valueOf(obj.getDate_creation()));
				prepare.setString(3, obj.getImage());
				prepare.setDouble(4, obj.getPrix());
				prepare.setString(5, obj.getCpu());
				prepare.setString(6, obj.getSe());
				prepare.setString(7, obj.getRam());
				prepare.setString(8, obj.getCarte_graphique());
				prepare.setString(9, obj.getHdd());
				prepare.setString(10,  obj.getNom());
				
				prepare.executeUpdate();  
					
			}
		catch (SQLException e) {
		e.printStackTrace();
		} 
	}
		
		
	// Recherche d'un Jeu par rapport à son id	
	@Override
	public Jeu read(String id_jeu) {
		
		Jeu leJeu = new Jeu();
		JeuDAO c = new JeuDAO();
		
		try {
			ResultSet result = this.connect.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
	        		  ResultSet.CONCUR_READ_ONLY)
	        		  .executeQuery("SELECT * FROM \"jeux_video\".jeu WHERE code = '" + id_jeu +"'");
	          
	          if(result.first())
	        	  leJeu = new Jeu(result.getInt("id_jeu"),result.getString("description"),result.getDate("date_creation").toLocalDate(),result.getString("image"),result.getDouble("prix"),result.getString("cpu"),result.getString("se"),result.getString("ram"),result.getString("carte_graphique"),result.getString("hdd"),result.getString("nom"));   
	    }
		catch (SQLException e) {
			        e.printStackTrace();
		}
		return leJeu;
		
	}
	
	// Mise à jour d'un Jeu
	@Override
	public void update(Jeu obj) {
		try { this .connect	
	               .createStatement( ResultSet.TYPE_SCROLL_INSENSITIVE,
	            		             ResultSet.CONCUR_UPDATABLE )
	               .executeUpdate("UPDATE \"jeux_video\".jeu SET nom = '" + obj.getNom() + "'"+
	                    	      " WHERE id_jeu = '" + obj.getId_Jeu()+"'" );
				
			  
		}
		catch (SQLException e) {
		      e.printStackTrace();
		}

	}


	// Suppression d'un Jeu
	@Override
	public void delete(String id_Jeu) {
		try {
	           this.connect
	               .createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
	            		            ResultSet.CONCUR_UPDATABLE)
	               .executeUpdate("DELETE FROM \"jeux_video\".jeu WHERE id_jeu = '" + id_Jeu +"'");
				
		 }
		catch (SQLException e) {
		            e.printStackTrace();
		}
	}

}
