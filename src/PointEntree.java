import controleurs.Controleur_principal;
import vues.Vue_principal;

// fichier qui lie tous (3 parties MVC) -> fichier à éxécuter
public class PointEntree {

	public static void main(String args[]) {
		
		// définit controleur
		Controleur_principal leControleur = new Controleur_principal();
		
		// définit la vue
		Vue_principal laVue = new Vue_principal (leControleur);
		
		leControleur.setVue_principal(laVue);
		
		laVue.setVisible(true);
	}
}

